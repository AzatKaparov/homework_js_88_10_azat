import './App.css';
import {Switch, Route} from "react-router-dom";
import Home from "./containers/Home/Home";
import Layout from "./components/UI/Layout/Layout";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import AddPost from "./containers/AddPost/AddPost";
import FullPost from "./containers/FullPost/FullPost";

function App() {
  return (
      <div className="App">
        <Layout>
          <Switch>
            <Route path="/" exact component={Home}/>
            <Route path="/posts/:id" exact component={FullPost}/>
            <Route path="/register" exact component={Register}/>
            <Route path="/login" exact component={Login}/>
            <Route path="/add-post" exact component={AddPost} />
            <Route render={() => <h1>Not found</h1>}/>
          </Switch>
        </Layout>
      </div>
  );
}

export default App;
