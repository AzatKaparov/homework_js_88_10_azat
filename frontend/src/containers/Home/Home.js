import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchPosts} from "../../store/actions/postsActions";
import PostPreview from "../../components/PostPreview/PostPreview";

const Home = () => {
    const dispatch = useDispatch();
    const { posts } = useSelector(state => state.posts);

    useEffect(() => {
        dispatch(fetchPosts());
    }, [dispatch]);

    const renderPosts = posts.map(post => <PostPreview
        key={post._id}
        title={post.title}
        description={post.description}
        image={post.image}
        date={post.created_at}
        author={post.author}
        id={post._id}
    />);

    return (
        <div className="container">
            {renderPosts}
        </div>
    );
};

export default Home;