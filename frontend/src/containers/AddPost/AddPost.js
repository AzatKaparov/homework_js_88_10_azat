import React, {useState} from 'react';
import {Button, Form} from 'react-bootstrap';
import {useDispatch, useSelector} from "react-redux";
import {createPost} from "../../store/actions/postsActions";
import Notification from "../../components/UI/Notification/Notification";

const AddPost = () => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.users);
    const [options, setOptions] = useState({
        title: "",
        description: "",
        image: "",
    });
    const error = useSelector(state => state.posts);

    let notif;
    if (!notif) {
        notif = null;
    } else {
        notif = <Notification show={!!error} text={error.message} type="Error"/>;
    }

    const formChangeHandler = e => {
        const value = e.target.value;
        const name = e.target.name;

        setOptions(prevState => ({
            ...prevState,
            [name]: [value]
        }))
    };

    const onFileChange = e => {
        const name = e.target.name;
        const file = e.target.files[0];

        setOptions(prevState => ({
            ...prevState,
            [name]: file
        }));
    };

    const onFormSubmit = async postData => {
        await dispatch(createPost(postData));
    };

    const submitFormHandler  = e => {
        e.preventDefault();
        const formData = new FormData();

        Object.keys(options).forEach(key => {
            formData.append(key, options[key]);
        });
        onFormSubmit(formData).catch(err => console.error(err));
    };

    return !!user && (
        <div className="container">
            {notif}
            <Form onSubmit={submitFormHandler}>
                <Form.Group className="mb-3" >
                    <Form.Label>Title</Form.Label>
                    <Form.Control type="text" name="title" onChange={formChangeHandler} />
                </Form.Group>
                <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                    <Form.Label>Description</Form.Label>
                    <Form.Control type="text" name="description" onChange={formChangeHandler} as="textarea" rows={5} />
                </Form.Group>
                <Form.Group>
                    <Form.Control
                        type="file"
                        onChange={onFileChange}
                        name="image"
                    />
                </Form.Group>
                <Button type="submit">Submit</Button>
            </Form>
        </div>
    );
};

export default AddPost;