import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Button, Form} from "react-bootstrap";
import {loginUser} from "../../store/actions/usersActions";
import Notification from "../../components/UI/Notification/Notification";
import {NavLink} from "react-router-dom";
import FormElement from "../../components/UI/FormElement/FormElement";

const Login = () => {
    const dispatch = useDispatch();
    const [user, setUser] = useState({
        username: "",
        password: "",
    });

    const loginError = useSelector(state => state.users.loginError);

    const inputChangeHandler = e => {
        const {name, value} = e.target;

        setUser(prevState => ({...prevState, [name]: value}));
    };

    const submitFormHandler = e => {
        e.preventDefault();
        dispatch(loginUser({...user}));
    };

    let errorNotif;
    if (loginError) {
        errorNotif = <Notification show={!!loginError} text={loginError.message} type="Error" />;
    } else {
        errorNotif = null;
    }


    return (
        <div className="container">
            {errorNotif}
            <Form onSubmit={submitFormHandler}>
                <FormElement
                    value={user.username}
                    name="username"
                    onChangeHandler={inputChangeHandler}
                    type="text"
                    required={true}
                    placeholder="Username"
                    label="Username"
                />
                <FormElement
                    value={user.password}
                    name="password"
                    onChangeHandler={inputChangeHandler}
                    type="password"
                    required={true}
                    placeholder="Password"
                    label="Password"
                />
                <Form.Text>
                    Don't have an account?
                    <NavLink to="/register">Register now</NavLink>
                </Form.Text>
                <Button type="submit" variant="primary">
                    Submit
                </Button>
            </Form>
        </div>
    );
};

export default Login;