import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchPost} from "../../store/actions/postsActions";
import defaultImg from "../../assets/default-post.jpg";
import {API_URL} from "../../constants";
import transformDate from "../../formatDate";
import AddCommentForm from "../../components/AddCommentForm/AddCommentForm";
import {fetchComments} from "../../store/actions/commentsActions";
import Comment from "../../components/Comment/Comment";

const FullPost = ({match}) => {
    const dispatch = useDispatch();
    const {post} = useSelector(state => state.posts);
    const {comments} = useSelector(state => state.comments);
    const user = useSelector(state => state.users.user);

    useEffect(() => {
        dispatch(fetchPost(match.params.id));
        dispatch(fetchComments(match.params.id));
    }, [match.params.id, dispatch]);

    return post && (
        <div className="container">
            <div className="FullPost">
                {post.image
                    ? <img alt="" src={`${API_URL}/uploads/${post.image}`}/>
                    : <img src={defaultImg} alt=""/>
                }
                <h1>{post.title}</h1>
                <p className="text-secondary my-0 ml-auto">
                    By {post.author ? post.author : "Anonymous"} at {transformDate(post.created_at)}
                </p>
                <h5 className="my-5">{post.description}</h5>
            </div>
            <div className="CommentForm">
                <h4>Add a comment</h4>
                <AddCommentForm/>
                {user
                    ? comments.map(item => (
                        <Comment key={item._id} text={item.comment} />
                    ))
                    : null
                }
            </div>
        </div>
    );
};

export default FullPost;