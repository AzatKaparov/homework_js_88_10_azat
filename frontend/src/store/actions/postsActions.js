import {axiosApi} from "../../axiosApi";
import {historyPush} from "./historyActions";

export const FETCH_POSTS_REQUEST = "FETCH_POSTS_REQUEST";
export const FETCH_POSTS_SUCCESS = "FETCH_POSTS_SUCCESS";
export const FETCH_POSTS_FAILURE = "FETCH_POSTS_FAILURE";

export const fetchPostsRequest = () => ({type: FETCH_POSTS_REQUEST});
export const fetchPostsSuccess = posts => ({type: FETCH_POSTS_SUCCESS, payload: posts});
export const fetchPostsFailure = err => ({type: FETCH_POSTS_FAILURE, payload: err});

export const fetchPosts = () => {
    return async dispatch => {
        dispatch(fetchPostsRequest());

        try {
            const response = await axiosApi.get(`/posts`);
            dispatch(fetchPostsSuccess(response.data));
        } catch (e) {
            dispatch(fetchPostsFailure(e));
        }
    };
};

export const CREATE_POST_REQUEST = "CREATE_POST_REQUEST";
export const CREATE_POST_SUCCESS = "CREATE_POST_SUCCESS";
export const CREATE_POST_FAILURE = "CREATE_POST_FAILURE";

export const createPostRequest = () => ({type: CREATE_POST_REQUEST});
export const createPostSuccess = post => ({type: CREATE_POST_SUCCESS, payload: post});
export const createPostFailure = err => ({type: CREATE_POST_FAILURE, payload: err});

export const createPost = data => {
    return async (dispatch, getState) => {
        dispatch(createPostRequest());

        try {
            const response = await axiosApi.post(`/posts`,
                data,
                {headers: {
                    "Authorization": getState().users.user.token
                    }}
            );
            dispatch(createPostSuccess(response.data));
            dispatch(historyPush("/"));
        } catch (error) {
            if (error.response && error.response.data) {
                dispatch(createPostFailure({message: "Looks like you are not authorized"}));
                throw error;
            } else {
                dispatch(createPostFailure({message: "No internet!"}));
                throw error;
            }
        }
    };
};

export const FETCH_POST_REQUEST = "FETCH_POST_REQUEST";
export const FETCH_POST_SUCCESS = "FETCH_POST_SUCCESS";
export const FETCH_POST_FAILURE = "FETCH_POST_FAILURE";

export const fetchPostRequest = () => ({type: FETCH_POST_REQUEST});
export const fetchPostSuccess = posts => ({type: FETCH_POST_SUCCESS, payload: posts});
export const fetchPostFailure = err => ({type: FETCH_POST_FAILURE, payload: err});

export const fetchPost = id => {
    return async dispatch => {
        dispatch(fetchPostRequest());

        try {
            const response = await axiosApi.get(`/posts/${id}`);
            dispatch(fetchPostSuccess(response.data));
        } catch (e) {
            dispatch(fetchPostFailure(e));
        }
    };
};