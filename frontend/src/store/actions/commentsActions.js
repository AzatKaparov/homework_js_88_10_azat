import {axiosApi} from "../../axiosApi";

export const FETCH_COMMENTS_REQUEST = "FETCH_COMMENTS_REQUEST";
export const FETCH_COMMENTS_SUCCESS = "FETCH_COMMENTS_SUCCESS";
export const FETCH_COMMENTS_FAILURE = "FETCH_COMMENTS_FAILURE";

export const fetchCommentsRequest = () => ({type: FETCH_COMMENTS_REQUEST});
export const fetchCommentsSuccess = posts => ({type: FETCH_COMMENTS_SUCCESS, payload: posts});
export const fetchCommentsFailure = err => ({type: FETCH_COMMENTS_FAILURE, payload: err});

export const fetchComments = post_id => {
    return async (dispatch, getState) => {
        dispatch(fetchCommentsRequest());

        try {
            const response = await axiosApi.get(`/comments?post=${post_id}`,
                {headers: {
                    "Authorization": getState().users.user.token
                    }}
                );
            dispatch(fetchCommentsSuccess(response.data));
        } catch (e) {
            dispatch(fetchCommentsFailure(e));
        }
    };
};