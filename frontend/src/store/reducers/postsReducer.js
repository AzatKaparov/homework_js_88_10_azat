import {
    CREATE_POST_FAILURE,
    CREATE_POST_REQUEST,
    CREATE_POST_SUCCESS, FETCH_POST_FAILURE, FETCH_POST_REQUEST, FETCH_POST_SUCCESS,
    FETCH_POSTS_FAILURE,
    FETCH_POSTS_REQUEST,
    FETCH_POSTS_SUCCESS
} from "../actions/postsActions";

const initialState = {
    loading: false,
    posts: [],
    error: null,
    post: null,
};

const postsReducer = (state=initialState, action) => {
    switch (action.type) {
        case FETCH_POSTS_REQUEST:
            return {...state, loading: true};
        case FETCH_POSTS_SUCCESS:
            return {...state, posts: action.payload, loading: false};
        case FETCH_POSTS_FAILURE:
            return {...state, error: action.payload, loading: false};
        case CREATE_POST_REQUEST:
            return {...state, loading: true};
        case CREATE_POST_SUCCESS:
            return {...state, posts: [...state.posts, action.payload], loading: false};
        case CREATE_POST_FAILURE:
            return {...state, error: action.payload, loading: false};
        case FETCH_POST_REQUEST:
            return {...state, loading: true};
        case FETCH_POST_SUCCESS:
            return {...state, loading: false, post: action.payload};
        case FETCH_POST_FAILURE:
            return {...state, loading: false, error: action.payload};
        default:
            return state;
    }
};

export default  postsReducer;