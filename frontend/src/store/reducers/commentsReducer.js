import {FETCH_COMMENTS_REQUEST, FETCH_COMMENTS_SUCCESS} from "../actions/commentsActions";
import {FETCH_POST_FAILURE} from "../actions/postsActions";

const initialState = {
    loading: false,
    comments: [],
    error: null,
};

const commentsReducer = (state=initialState, action) => {
    switch (action.type) {
        case FETCH_COMMENTS_REQUEST:
            return {...state, loading: true};
        case FETCH_COMMENTS_SUCCESS:
            return {...state, loading: false, comments: action.payload};
        case FETCH_POST_FAILURE:
            return {...state, loading: false, comments: action.payload};
        default:
            return state;
    }
};

export default commentsReducer;