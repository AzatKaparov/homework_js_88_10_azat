export const monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];

const transformDate = (date) => {
    const initialDate = new Date(date);
    initialDate.toLocaleString("default", {month: 'long'});
    const year = initialDate.getFullYear();
    const month = monthNames[initialDate.getMonth()];
    const day = initialDate.getUTCDate() + 1;
    const hour = initialDate.getHours();
    const minute = initialDate.getMinutes();
    return `${day} ${month} ${year}, ${hour}:${minute < 10 ? `0${minute}` : minute}`;
};

export default transformDate;