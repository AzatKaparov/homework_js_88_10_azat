import React, {useState} from 'react';
import {Button, Form} from "react-bootstrap";
import {useDispatch} from "react-redux";

const AddCommentForm = () => {
    const dispatch = useDispatch();
    const [comment, setComment] = useState("");

    const formChangeHandler = e => {
        setComment(e.target.value);
    };

    const formSubmitHandler = e => {
        e.preventDefault();
        // dispatch()
    };


    return (
        <div className="AddCommentForm">
            <Form onSubmit={formSubmitHandler} className="text-left border p-3">
                <Form.Group className="mb-3" >
                    <Form.Label>Comment's text</Form.Label>
                    <Form.Control
                        type="text"
                        name="title"
                        onChange={formChangeHandler}
                        value={comment}
                    />
                </Form.Group>
                <Button type="submit">Submit</Button>
            </Form>
        </div>
    );
};

export default AddCommentForm;