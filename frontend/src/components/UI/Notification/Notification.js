import React from 'react';
import {Col, Toast} from "react-bootstrap";

const Notification = ({show, text, type}) => {
    return (
            <Col className="mb-2" xs={6}>
                <Toast show={show} delay={3000} autohide>
                    <Toast.Header>
                        <strong className="me-auto">{type}</strong>
                    </Toast.Header>
                    <Toast.Body>{text}</Toast.Body>
                </Toast>
            </Col>
    );
};

export default Notification;