import React from 'react';
import NavItem from "../NavItem/NavItem";
import {useDispatch} from "react-redux";
import {logoutUser} from "../../../store/actions/usersActions";
import {Button} from "react-bootstrap";

const UserMenu = () => {
    const dispatch = useDispatch();
    const logout = () => {dispatch(logoutUser())};

    return (
        <>
            <NavItem to="/add-post" title="Add post" />
            <Button onClick={logout} variant="warning">Logout</Button>
        </>
    );
};

export default UserMenu;