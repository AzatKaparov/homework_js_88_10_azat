import React from 'react';
import defaultImg from '../../assets/default-post.jpg';
import {API_URL} from "../../constants";
import "./PostPreview.css";
import transformDate from "../../formatDate";
import {useSelector} from "react-redux";
import {NavLink} from "react-router-dom";

const PostPreview = ({title, image, description, author, date, id}) => {
    const {user} = useSelector(state => state.users);
    let imgSrc;
    if (!image || !user) {
        imgSrc = defaultImg;
    } else {
        imgSrc = `${API_URL}/uploads/${image}`;
    }

    return (
        <div className="PostPreview d-flex w-100 mb-4">
            <div className="img-block">
                <img src={imgSrc} alt=""/>
            </div>
            <div className="d-flex p-3 flex-column w-100 align-items-start justify-content-between">
                <h3>{title}</h3>
                {user ? <p>{description}</p> : null}
                <NavLink to={`posts/${id}`}>More</NavLink>
                <p className="text-secondary my-0 ml-auto">
                    By {author ? author : "Anonymous"} at {transformDate(date)}
                </p>
            </div>
        </div>
    );
};

export default PostPreview;