import React from 'react';

const Comment = ({text, author}) => {
    return (
        <div className="Comment w-100 text-left border my-2 p-3">
            <h5>{author}</h5>
            <p>{text}</p>
        </div>
    );
};

export default Comment;