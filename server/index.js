const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const users = require('./app/users');
const posts = require('./app/posts');
const comments = require('./app/comments');

const app = express();
const port = 8000;

app.use(express.json());
app.use(cors());
app.use(express.static('public'));

app.use('/users', users);
app.use('/posts', posts);
app.use('/comments', comments);

const run = async () => {
    await mongoose.connect('mongodb://localhost/forum');

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });

    process.on('exit', async () => {
        console.log('exiting');
        await mongoose.disconnect();
    });
};

run().catch(e => console.error(e));