const mongoose = require('mongoose');
const Shema = mongoose.Schema;

const PostShema = new Shema({
    title: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: () => {
            return !!this.image;
        }
    },
    image: {
        type: String,
        required: () => {
            return !!this.description;
        }
    },
    author: {
        type: String,
        default: "Anonymous",
    },
    created_at: {
        type: Date,
        default: () => Date.now() + 7*24*60*60*1000,
    }
});

const Post = mongoose.model("Post", PostShema);
module.exports = Post;