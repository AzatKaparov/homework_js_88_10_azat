const mongoose = require('mongoose');
const Shema = mongoose.Schema;

const CommentShema = new Shema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Post",
        required: true,
    },
    post: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user",
        required: true
    },
    comment: {
        type: String,
        required: true,
    }
});

const Comment = mongoose.model("Comment", CommentShema);
module.exports = Comment;