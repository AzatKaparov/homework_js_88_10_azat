const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const Post = require('../models/Post');
const auth = require('../middleware/auth');


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});
const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const result = await Post.find().sort({created_at: -1});
        if (!result || result.length === 0) {
            return res.status(404).send({message: "Not found any posts"});
        }
        res.status(200).send(result);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const post =  await Post.findById(req.params.id);
        if (!post || post.length === 0) {
            return res.status(400).send({message: "Not found"});
        } else {
            res.send(post);
        }
    } catch (e) {
        res.sendStatus(500);
    }
});

router.post('/', [auth, upload.single('image')], async (req, res) => {
    if (!req.body.title || req.body.title === "") {
        return res.status(400).send({message: "Data not valid!"});
    }
    if (!req.body.description || req.body.description === "") {
        return res.status(400).send({message: "Data not valid!"});
    }

    const postData = {
        title: req.body.title,
        description: req.body.description,
        author: req.user.username,
    };

    if (req.file) {
        postData.image = req.file.filename;
    }

    const post = new Post(postData);

    try {
        await post.save();
        return res.status(200).send(post);
    } catch (error) {
        return res.status(400).send(error);
    }
});



module.exports = router;