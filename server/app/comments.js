const express = require('express');
const Post = require('../models/Post');
const Comment = require('../models/Comment');
const auth = require('../middleware/auth');


const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const query = {};
        let result;
        if (req.query.post) {
            query.post = req.query.post;
            try {
                result = await Comment.find({post: query.post});
            } catch (e) {
                return res.status(404).send({message: e.message});
            }
        }
        if (!result || result.length === 0) {
            return res.status(404).send({message: "Not found"});
        }
        res.status(200).send(result);
    } catch (e) {
        return res.status(500).send({message: e});
    }
});


router.post('/', auth, async (req, res) => {
    if (!req.body.comment || req.body.comment === "") {
        return res.status(400).send({message: "Data not valid!"});
    }

    const commentData = {
        comment: req.body.comment,
    };

    const comment = new Post(commentData);

    try {
        await comment.save();
        return res.status(200).send(comment);
    } catch (error) {
        return res.status(400).send(error);
    }
});



module.exports = router;