const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const Post = require('./models/Post');
const Comment = require('./models/Comment');
const User = require('./models/User');

const run = async () => {
    await mongoose.connect(config.database);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [post1, post2] = await Post.create({
        title: "Test 1",
        description: "Test 1",
        author: "Someone",
    }, {
        title: "Test 2",
        description: "Test 2"
    });

    const [user1, user2] = await User.create({
        username: 'admin',
        password: 'admin',
        token: nanoid(),
    }, {
        username: 'root',
        password: 'root',
        token: nanoid(),
    });

    await Comment.create({
        user: user1,
        comment: "test comment",
        post: post1,
    }, {
        user: user1,
        comment: "test comment 2",
        post: post1,
    }, {
        user: user1,
        comment: "test comment 3",
        post: post1,
    }, {
        user: user2,
        comment: "test comment 3",
        post: post2,
    }, {
        user: user2,
        comment: "test comment 4",
        post: post2,
    }, {
        user: user2,
        comment: "test comment 5",
        post: post2,
    });

    await mongoose.connection.close();
};

run().catch(console.error);